/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function () {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function () {
        app.receivedEvent('deviceready');
        var configOptions = {
            apiKey: "<api-key>",
            server: "https://interactor.swisscom.ch",
            reduceServerTraffic: true,
            logging: true,
            checkBluetoothAccess: true,                //optional, iOS only
            checkLocationAccess: true,                 //optional, iOS only
            backgroundScanInterval: 10000,             //optional, Android only
            foregroundScanInterval: 8000               //optional, Android only
        };
        var configNotification = {
            receiveDuring: Interactor.NotificationStates.BOTH, //optional
            stackText: false                          //optional, Android only
        };

        Interactor.interactorRunning(
            function (result) {
                var interactorRunning = result["interactorRunning"];
                console.log("Interactor running: " + interactorRunning);
                if (!interactorRunning) {
                    Interactor.configureInteractor(configOptions,
                        function(cb){
                            console.log("Success: configure Interactor");
                            Interactor.configureNotification(configNotification,
                                function(cb){
                                    console.log("Success: configure Notification");
                                    Interactor.startInteractor(function(cb){
                                        console.log("Success: start Interactor");
                                    },app.errorCallback);
                                },
                                app.errorCallback);
                        },
                        app.errorCallback);
                }
            }, app.successCallback, app.errorCallback
        );
        Interactor.getVersionInfo(
            function (cb) {

                var sdkVersion = cb.interactorSDKVersion;
                var cordovaVersion = cb.cordovaInteractorPluginVersion;

                console.log("Version Interactor SDK: " + sdkVersion);
                console.log("Version Interactor Cordova Plugin: " + cordovaVersion);

                var versionSDKField = document.getElementById('version_sdk_interactor');
                var versionCordovaPluginField = document.getElementById('version_cordova_plugin');

                var nodeSDK = document.createElement('span');
                nodeSDK.innerHTML = sdkVersion;

                var nodePlugin = document.createElement('span');
                nodePlugin.innerHTML = cordovaVersion;

                versionSDKField.appendChild(nodeSDK);
                versionCordovaPluginField.appendChild(nodePlugin);

            }, app.successCallback, app.errorCallback);
        Interactor.registerEventListener(
            function (event) {
                console.log('Event-listener called for ' + event.name);
                var output = document.getElementById('output');
                var node = document.createElement('p');
                node.innerHTML = event.name;
                output.appendChild(node);
            }, app.successCallback, app.errorCallback);
        Interactor.registerNotificationListener(
            function (event) {
                console.log("Anzahl Events " + event.notifications.length);
                var output = document.getElementById('output');
                var node = document.createElement('p');
                node.innerHTML = "Notification Clicked - PP " + event.notifications[0].name;
                output.appendChild(node);
            }, app.successCallback, app.errorCallback);
    },
    // Update DOM on a Received Event
    receivedEvent: function (id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    },

    successCallback: function () {
        console.log('success');
    },

    errorCallback: function (error) {
        if (error) {
            console.log('error: ' + JSON.stringify(error));
        }
    }
};

app.initialize();
