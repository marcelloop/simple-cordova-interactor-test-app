# simple-cordova-interactor-test-app

####clone repo

```
git clone https://bitbucket.org/marcelloop/simple-cordova-interactor-test-app.git
cd simple-cordova-interactor-test-app
```

####install cordova
```
npm install -g cordova
```

####add platform
```
cordova platform add android
cordova platform add ios
```

####add cordova plugins
```
cordova plugin add cordova-plugin-whitelist
cordova plugin add cordova-plugin-app-event
cordova plugin add https://bitbucket.org/interactdev/cordova-interactor-plugin-public.git
```

####add your api key
in ```<project-root>/www/js/index.js``` add your api key or [other configuration](https://interactor-swisscom.readme.io/docs/getting-started-with-cordova)

```
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        var configOptions = {
            apiKey: "<api-key>",
            server: "https://interactor.swisscom.ch",
            logging: true,
            checkBluetoothAccess: true,                //optional, iOS only
            checkLocationAccess: true,                 //optional, iOS only
            backgroundScanInterval: 10000,             //optional, Android only
            foregroundScanInterval: 8000               //optional, Android only
        };
        var configNotification = {
            receiveDuring: Interactor.NotificationStates.BOTH, //optional
            stackText: false                          //optional, Android only
        };
},
```

####build on android
```
cordova build android
cordova run android
```

####build on ios
```
cordova build ios
```
now you have to open ```<project-root>/platforms/ios/HelloCordova.xcodeproj``` with xcode and build the project